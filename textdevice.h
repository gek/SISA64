

#include "sisa64.h"
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
static struct termios oldChars;
static struct termios newChars;
#define SCREEN_WIDTH_CHARS 1024
#define SCREEN_HEIGHT_CHARS 768
static unsigned char stdout_buf[
    (SCREEN_WIDTH_CHARS * SCREEN_HEIGHT_CHARS)
     + SCREEN_WIDTH_CHARS
] = {0};


static void initTermios(int echo)
{
  tcgetattr(STDIN_FILENO, &oldChars); /* grab old terminal i/o settings */
  newChars = oldChars; /* make new settings same as old settings */
  newChars.c_lflag &= ~ICANON; /* disable buffered i/o */
  newChars.c_lflag &= echo ? ECHO : ~ECHO; /* set echo mode */
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &newChars); /* use these new terminal i/o settings now */
}


static void dieTermios(){
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &oldChars); /* use these new terminal i/o settings now */	
}
