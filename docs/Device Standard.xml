<?xml version="1.0" encoding="UTF-8"?>
<indexing>
 <object alt="" name="Image1" object_type="graphic"/>
 <paragraph index="12" node_type="writer">S-ISA-64 Device Standard</paragraph>
 <object index="14" name="Table of Contents1" object_type="section"/>
 <object index="15" name="Table of Contents1_Head" object_type="section"/>
 <paragraph index="16" node_type="writer" parent_index="15">Contents</paragraph>
 <paragraph index="18" node_type="writer" parent_index="14">Overview	3</paragraph>
 <paragraph index="19" node_type="writer" parent_index="14">Prerequisite Terminology and Concepts	4</paragraph>
 <paragraph index="20" node_type="writer" parent_index="14">Device Interaction- Execution Flow	5</paragraph>
 <paragraph index="21" node_type="writer" parent_index="14">Default Device Behavior	8</paragraph>
 <paragraph index="22" node_type="writer" parent_index="14">Peripheral Enabled Table	13</paragraph>
 <paragraph index="23" node_type="writer" parent_index="14">Secondary Device Standard Layout	14</paragraph>
 <paragraph index="24" node_type="writer" parent_index="14">Peripheral Address Space Layout	15</paragraph>
 <paragraph index="27" node_type="writer">Overview</paragraph>
 <paragraph index="28" node_type="writer">S-ISA-64 accesses non-RAM components through two instructions: the dread and dwrite instructions, which send data and commands on a special bus dedicated for interacting with peripheral components.</paragraph>
 <paragraph index="30" node_type="writer">The Programmer’s Manual covers the overall functionality of these two instructions in the instruction set, while this document aims to document how components on the system (such as non-volatile storage, a graphics card, keyboard, display, microphone, ethernet controller, or USB hub). Are discovered and interacted with in terms of dread and dwrite.</paragraph>
 <paragraph index="32" node_type="writer">It also documents the errata of multi-threaded access to the device bus and what interactions must be implemented.</paragraph>
 <paragraph index="34" node_type="writer">The Programmer’s Manual is considered a Necessary Companion for understanding this document.</paragraph>
 <paragraph index="37" node_type="writer">Prerequisite Terminology and Concepts</paragraph>
 <paragraph index="38" node_type="writer">This is what this whole document is actually about:</paragraph>
 <paragraph index="40" node_type="writer">dread- instruction in the S-ISA-64 instruction set which reads 64 bits from a device on the device bus- a special address space which behaves like memory that can be read or written to. The actual instruction takes a single register ID operand and writes the return value to the same, but the effective functionality is to take in an address and return a 64 bit integer. Thus, the prototype is</paragraph>
 <paragraph index="42" node_type="writer">uint64 dread(uint64 address);
</paragraph>
 <paragraph index="43" node_type="writer">dwrite- instruction in the S-ISA-64 instruction set which writes 64 bits to a device on the device bus. The actual instruction takes two register ID’s as operands, but the effective functionality is to pass in an address and a value, and return nothing. Thus, the prototype is:</paragraph>
 <paragraph index="45" node_type="writer">void dwrite(uint64 address, uint64 val);</paragraph>
 <paragraph index="47" node_type="writer">These two operations form the basis of all interactions with components (excluding main memory) on the system. Components on the system may also be configured (through dwrite operations) to take data from main memory, if supported. (How devices with such functionality behave and how they are configured shall be documented in this standard.)</paragraph>
 <paragraph index="49" node_type="writer">This document’s primary goal is to create a hardware-software interaction standard for the S-ISA-64 architecture before hundreds of hours of work has been spent to design the actual hardware, the emulator, operating system development kit, and system libraries.</paragraph>
 <paragraph index="51" node_type="writer">Unless otherwise specified, Anything called an “address” in this document is an address on the device bus and not a main-memory address.</paragraph>
 <paragraph index="52" node_type="writer">Device Interaction- Execution Flow</paragraph>
 <paragraph index="53" node_type="writer">Upon boot, the operating system on the computer will check its own memory size by reading address 1, check for the presence of standards-compliant coprocessor cores in address 3, and mutual exclusion devices (“mutexes”) in address 5. Using these values, if applicable, it may read the pointer to the processor kickstart addresses in address 4. After reading 1 and 4, the operating system may also try to use one of the processor kickstart addresses to start a secondary core. How processor kickstart addresses work shall be documented later. It may also read the pointer to the Mutual Exclusion Device Addresses in Address 6. It may also try to retrieve the starting address of Processor Status Variables from Address 7.</paragraph>
 <paragraph index="54" node_type="writer">Here, we have our first standard rules:</paragraph>
 <paragraph index="55" node_type="writer">1. The memory size and millisecond clock functionalities of addresses 1 and 2 are considered vital to a system and shall not be absent or mangled in a compliant implementation. </paragraph>
 <paragraph index="56" node_type="writer">2. Any default device addresses which are not implemented as according to this standard, shall safely return zero, or do nothing on a write.</paragraph>
 <paragraph index="58" node_type="writer">Finally, and most importantly, the operating system may check for attached devices by checking address 8, and if the return value is non-zero it may check the 0x1000000 device enabled table, and the individual address spaces of the available peripherals, especially Address 0, which is the “DeviceType”.</paragraph>
 <paragraph index="60" node_type="writer">To enable this functionality...</paragraph>
 <paragraph index="62" node_type="writer">3. If there are no secondary peripherals on the system, address 8 returns 0.</paragraph>
 <paragraph index="63" node_type="writer">4. If the return value of address 8 is non-zero, then reading the first addresses in the address spaces of the corresponding peripherals must be defined behavior.</paragraph>
 <paragraph index="64" node_type="writer">5. It shall never result in a system crash, random shutdown, or other “unsafe” behavior to read the first 256 bytes within the address space of any peripheral other than the default device (0).</paragraph>
 <paragraph index="65" node_type="writer">6. 1,048,576 addresses starting at 0x1000000 must return 1 or 0 corresponding to whether that peripheral ID currently has a functioning peripheral in it. For addresses not corresponding to the number of peripherals supported by the system, the return value is always zero.</paragraph>
 <paragraph index="67" node_type="writer">After power on, the operating system may periodically check the Device Enabled Table, and the Device Types of each and every peripheral on the system, so that it can update what services it can provide to user programs.</paragraph>
 <paragraph index="69" node_type="writer">The User may decide to swap components on the system. For some critical devices, this would most likely require a power off. However, some devices, like keyboards, mice, touchscreens, microphones, gamepads, and flash storage devices may be disconnected or connected at runtime. </paragraph>
 <paragraph index="71" node_type="writer">Properly disconnecting peripherals in a way that prevents the Operating System from performing undefined behavior may require notifying the operating system by the user, however connecting a new peripheral should never change the functionality of other devices. It should not, for instance, cause a still-plugged-in device’s Peripheral ID to change, which might cause a write operating in-progress to be moved from a USB stick to a hard drive.</paragraph>
 <paragraph index="73" node_type="writer">Reading and writing to a disabled peripheral is undefined behavior that may result in hardware damage. Reading and writing to a peripheral in a way in which it was not designed to be read from or written to is undefined behavior.</paragraph>
 <paragraph index="75" node_type="writer">On a real system, then, a Peripheral ID corresponds more to a physical expansion port on the system, rather than the peripheral itself.</paragraph>
 <paragraph index="77" node_type="writer">The implementation is required to keep track of the number of active, functioning peripherals on a system at any given time and where they are on the system.</paragraph>
 <paragraph index="79" node_type="writer">What types of peripherals and ports are considered “safe” to hot-swap at a given time is implementation defined. How an operating system handles hot-swapped peripherals to prevent itself from executing undefined behavior is implementation defined.</paragraph>
 <paragraph index="81" node_type="writer">Other cores on the system may also access the device bus, however accessing the same device asynchronously is not necessarily safe unless the device indicates that it is thread-safe (Having atomic-style behavior on a read/write).</paragraph>
 <paragraph index="82" node_type="writer">Default Device Behavior</paragraph>
 <paragraph index="83" node_type="writer">The default device, Peripheral 0, is the heart of the implementation and is vital to the functionality of any operating system.</paragraph>
 <paragraph index="85" node_type="writer">Serial I/O UART (Address 0)</paragraph>
 <paragraph index="86" node_type="writer">The Serial I/O device provides blocking read/write of ASCII character values for debugging purposes. It is not considered essential for a S-ISA-64 implementation as its primary purpose is debugging, but this address must only be used for that purpose. When dread is invoked on the address, the processor is halted until a value is retrieved from the input device. An 8-bit ASCII character (the bottom 7 being the 7-bit ASCII standard) value is returned in the bottom 8 bits of the 64 bit return value. When dwrite is invoked on the address, the bottom 8 bits of the input are treated as an 8-bit ASCII value, and should be displayed, transmitted, or broadcast as such to the appropriate serial port.</paragraph>
 <paragraph index="88" node_type="writer">Memory Size (Address 1)</paragraph>
 <paragraph index="89" node_type="writer">The Memory Size query device returns the amount of available system RAM for the implementation. Writing to this address is undefined behavior, but reading it shall return the number of available addressable contiguous bytes to the S-ISA-64 processor starting at address zero.</paragraph>
 <paragraph index="91" node_type="writer">Clock (Address 2)</paragraph>
 <paragraph index="92" node_type="writer">The Clock device returns a measure of the number of milliseconds passed since some point in the past to the caller on a dread. It must provide at least 32 bits of wrap-around (It may wrap around every 4,294,967.296 seconds).</paragraph>
 <paragraph index="95" node_type="writer">Processor Count (Address 3)</paragraph>
 <paragraph index="96" node_type="writer">The processor count device returns the number of available compliant processors on the system, which is dubbed Np. If the implementation is single-threaded, it may return either 0 (unimplemented) or 1. The behavior of Processors shall be documented in Multiprocessor Essentials.</paragraph>
 <paragraph index="98" node_type="writer">Processor Kickstart Address Array Pointer (Address 4)</paragraph>
 <paragraph index="100" node_type="writer">Reading address 4 shall return a valid device bus address (Dubbed “Kickstart[0]”), which, if written to, attempts to start the first processor on the system (Which is the boot processor). The next address (“Kickstart[1]”) attempts to start the second processor on the system, if available, and so on, up to Kickstart[Np-1] where Np is the number of compliant processors on the system, as provided by address 3.</paragraph>
 <paragraph index="102" node_type="writer">If there are no available kickstart addresses, or they are unimplemented, Address 4 shall return 0.</paragraph>
 <paragraph index="104" node_type="writer">If reading address 3 returns 0, then reading address 4 must also return 0.</paragraph>
 <paragraph index="106" node_type="writer">Mutex Count (Address 5)</paragraph>
 <paragraph index="108" node_type="writer">Address 5 shall return the number of available contiguous mutual exclusion devices on the system, which is dubbed Nm. If none are available, zero is returned.</paragraph>
 <paragraph index="111" node_type="writer">Mutual Exclusion Device Array Pointer (Address 6)</paragraph>
 <paragraph index="113" node_type="writer">Address 6 shall return a valid device bus address (Dubbed “Mutex[0]”), which, if a value of 0 is written to it, shall attempt to “unlock” it. And if a value of 1 is written to it, shall attempt to “lock” it. Writing other values is implementation defined behavior. The next address, (“Mutex[1]”) shall behave identically to Mutex[0] if Nm is at least 2. </paragraph>
 <paragraph index="115" node_type="writer">If reading address 5 returns 0, then address 6 must also return 0.</paragraph>
 <paragraph index="117" node_type="writer">Processor Status Variable Array Pointer (Address 7)</paragraph>
 <paragraph index="119" node_type="writer">Reading Address 7 shall return a valid device bus address (Dubbed “Pstat[0]”) which, if read, shall return a value indicating whether or not the processor is running and/or currently unavailable to be kickstarted by writing to its corresponding Kickstart address (which in this case would be Kickstart[0])</paragraph>
 <paragraph index="121" node_type="writer">If the processor is running, and not able to be kickstarted, the value returned shall be 1.</paragraph>
 <paragraph index="122" node_type="writer">if the processor is available to be kickstarted, the value returned shall be 0.</paragraph>
 <paragraph index="123" node_type="writer">if the processor is for any reason unable to be kickstarted, the value returned shall be 0.</paragraph>
 <paragraph index="125" node_type="writer">The next address, Pstat[1] shall behave identically to Pstat[0] if available.</paragraph>
 <paragraph index="127" node_type="writer">The number of processor status variables, if implemented, must be at least as large as the number of processors, Np.</paragraph>
 <paragraph index="129" node_type="writer">If the return value of Address 3 is zero, the return value of Address 7 must also be zero.</paragraph>
 <paragraph index="130" node_type="writer">Peripheral Count (Address 8)</paragraph>
 <paragraph index="131" node_type="writer">Address 8 shall yield the number of secondary devices (Dubbed “NeX”) on the system (Dubbed “Peripherals”). Each secondary device (Peripherals 1 – NeX) has a series of attributes stored at the beginning of its address space. If only the default device is available, the return value is 0.</paragraph>
 <paragraph index="133" node_type="writer">Peripherals being plugged in or unplugged from a system does not change after power-on, rather, they just become disabled.</paragraph>
 <paragraph index="137" node_type="writer">Pointer to Main Memory Mirror (Address 9)</paragraph>
 <paragraph index="138" node_type="writer">Returns Device Bus Address where a 64-bit-aligned mirror of main memory can be accessed. This exists to allow DMA controllers to read and write on the device address bus rather than using main memory.</paragraph>
 <paragraph index="140" node_type="writer">Set Is Serial NonBlocking (Address 10)</paragraph>
 <paragraph index="141" node_type="writer">Reading this will reveal whether Serial is nonblocking or not (0 or 1)</paragraph>
 <paragraph index="142" node_type="writer">Writing 0 will make it blocking, and 1 will make it non-blocking.</paragraph>
 <paragraph index="144" node_type="writer">Set System Time (Address 11)</paragraph>
 <paragraph index="145" node_type="writer">Reading this will reveal whether the system time can be set (0 or 1).</paragraph>
 <paragraph index="146" node_type="writer">Writing will set the system time in milliseconds to the value written.</paragraph>
 <paragraph index="148" node_type="writer">Default Device Vendor String Pointer (Address 12)</paragraph>
 <paragraph index="149" node_type="writer">Returns a Device Bus Address where a null-terminated ASCII string specifying vendor-specific information (vendor name, model, extensions, whatever) can be read. Vendor strings are read 8 bits at a time, so contiguous addresses yield increasing bytes.
If the vendor string started at 0x100 and the string was “Hi!” then reads would look like this:</paragraph>
 <paragraph index="151" node_type="writer">read(0x100) → ‘H’</paragraph>
 <paragraph index="152" node_type="writer">read(0x101) → ‘i’</paragraph>
 <paragraph index="153" node_type="writer">read(0x102) → ‘!’</paragraph>
 <paragraph index="154" node_type="writer">read(0x103) → 0</paragraph>
 <paragraph index="156" node_type="writer">This model of how a “vendor string” works shall be mimicked by all compliant secondary devices.</paragraph>
 <paragraph index="157" node_type="writer">Peripheral Enabled Table</paragraph>
 <paragraph index="158" node_type="writer">At address 0x1000000 on the device bus, a series of boolean values can be queried to indicate whether an associated peripheral is enabled. The values correspond to the peripherals 0-NeX. Peripheral 0 is always available so it is locked at 1. If peripheral 1 is enabled, 0x1000001 will also yield a 1.</paragraph>
 <paragraph index="160" node_type="writer">This is, essentially, how plug-and-play is handled. If a device is unplugged, it become disabled. The peripheral count does not change during the runtime of the computer. However, a peripheral may change functionality or become disabled during runtime, and if that happens, its boolean value in this table will be zero.</paragraph>
 <paragraph index="162" node_type="writer">In order to tell what type a device is, the device must itself be queried from its address space.</paragraph>
 <paragraph index="163" node_type="writer">Secondary Device Standard Layout</paragraph>
 <paragraph index="164" node_type="writer">Peripherals on a system are identified by a peripheral ID, from 0 (the default device) to NeX (inclusive), which has a maximum value of 1,048,575.</paragraph>
 <paragraph index="166" node_type="writer">Any address on peripheral 1, then, looks like this in bits:
0000 0000 0000 0000 0001 XXXX XXXX XXXX (…)</paragraph>
 <paragraph index="168" node_type="writer">Where (…) is filled in with X’s corresponding to the remaining bits of the address.</paragraph>
 <paragraph index="170" node_type="writer">Address 1 on peripheral 1 replaces all those X’s with zeroes, except for the very last X, which is a 1.</paragraph>
 <paragraph index="171" node_type="writer">To safely convert from a Peripheral ID and a Peripheral Address to a Device Bus address, you do the following transformation...</paragraph>
 <paragraph index="172" node_type="writer">in C pseudocode...</paragraph>
 <paragraph index="174" node_type="writer">dev_addr = </paragraph>
 <paragraph index="175" node_type="writer">	((uint64_t)peripheral_ID &lt;&lt; 44) </paragraph>
 <paragraph index="176" node_type="writer">	| </paragraph>
 <paragraph index="177" node_type="writer">	(peripheral_addr &amp; 0xFffFFffFFff);</paragraph>
 <paragraph index="178" node_type="writer">where dev_addr is the resulting device bus address, Peripheral ID is the ID of the peripheral (0 being the default device) and peripheral_addr being the address within that peripheral’s address space.</paragraph>
 <paragraph index="180" node_type="writer">Peripheral Address Space Layout</paragraph>
 <paragraph index="182" node_type="writer">Within a peripheral’s address space, the following addresses shall  be reserved, and required to be implemented, for the following purposes:</paragraph>
 <paragraph index="183" node_type="writer">Address 0- Device Type. (Read only- Write is UB)</paragraph>
 <paragraph index="184" node_type="writer">	This address shall yield a Device Type ID which tells the operating 	system what type of device this peripheral is. The specification of 	Device Type IDs shall be given later. 0 is returned if the device is 	disabled. 0xFFffFFffFFffFFff is returned if the device does not use a 	device type ID.</paragraph>
 <paragraph index="185" node_type="writer">Address 1- Device Vendor ID (Read only- Write is UB)</paragraph>
 <paragraph index="186" node_type="writer">	This address shall yield a Vendor ID. Vendor ID 0 is “None”. The list of 	reserved Vendor IDs shall be listed later.</paragraph>
 <paragraph index="187" node_type="writer">Address 2- Pointer to Device Vendor String (Read only- Write is UB)</paragraph>
 <paragraph index="188" node_type="writer">	Similar to the Default Device’s vendor string system. Returns a 	peripheral address you can read sequentially within the peripheral’s 	address space (Not prefixed, the top 20 bits are empty) to get an ascii 	string identifying various vendor-specific properties of the peripheral as 	ASCII text. The string is null-terminated.</paragraph>
 <paragraph index="189" node_type="writer">Address 3- Write-to-Disable (Read/Write)</paragraph>
 <paragraph index="190" node_type="writer">	Reading this address says whether or not the peripheral can be disabled, 	in preparation for it being safely removed from a system during 	runtime. If the return value is ‘0’ then writing to this address does 	nothing, but if the return value is ‘1’ then writing will cause the 	peripheral to eventually disable itself. The OS should regularly check 	  	the appropriate entry in the Device Enabled Table to discover when the 	device has safely disabled itself so that the user’s unplugging the device 	does not result in undefined behavior.</paragraph>
 <paragraph index="191" node_type="writer">Address 4- Is Thread Safe.</paragraph>
 <paragraph index="192" node_type="writer">	Reading this address says whether or not the peripheral is thread-safe. If  	every single functional address on the device can be read from or 	written to atomically, then this returns 1. Otherwise, reads and writes 	must be guarded by a mutex, or only ever happen on a single core.</paragraph>
 <paragraph index="193" node_type="writer">The rest of the address space is free for the peripheral to implement.</paragraph>
</indexing>
