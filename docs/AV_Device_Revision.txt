AV_DEVICE REVISION PLAN

The AV device currently uses entirely software for both graphics and sound.

It should be re-worked to use OpenGL and OpenAL (or similar APIs) to drive graphics and audio.

This would make the emulated device more "realistic".

NEEDED FEATURES:

AUDIO:
    - Streams, preferably at least 8
        - Location information
    - Sound effects, preferably at least 8. Allow pre-loading buffer contents
        - Location information
    - Hearer location information
    - Need to be able to stop and start streams and sound effects.
    - Want 3d audio
    - Want sound environments
VIDEO:
    - Defineable resolution
        - Query resolution command
    - Draw filled colored triangles; meshes
        - Transform stack
        - Set material
        - Create display list -> draw lots of triangles quickly
    - Hardware Lights for triangle rendering
        - Point lights, at least 8
        - Directional lights, at least 1
        - Dot product diffuse at least, specular optional
    - Draw scaled/rotated sprite command(s)
        - ?Transparency?
        - ?Transform stack?
    - Draw text commands
        - ?Transparency?


